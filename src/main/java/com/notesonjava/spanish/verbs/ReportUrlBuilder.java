package com.notesonjava.spanish.verbs;

import java.util.Collection;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ReportUrlBuilder {
	
	private String reportUrl;
	
	public String report(){
		return reportUrl + "/verbs/report";
	}

	public String report(Collection<String> verbs){
		StringBuilder sb = new StringBuilder();
		sb.append(reportUrl).append("/verbs/report");
		if(!verbs.isEmpty()) {
			sb.append("?");
			verbs.forEach(v -> sb.append("verb=").append(v).append("&"));
		}
		
		return sb.toString();
	}

	
	public String dailyReport(){
		return reportUrl + "/verbs/report/daily";
	}

	public String dailyReport(int numDays){
		return reportUrl + "/verbs/report/daily?numDays=" + numDays;
	}

}
