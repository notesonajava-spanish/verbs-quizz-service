package com.notesonjava.spanish.verbs;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.spanish.dto.VerbAnswerDto;
import com.notesonjava.spanish.model.VerbAnswer;
import com.notesonjava.spanish.model.VerbQuestion;
import com.rabbitmq.client.Channel;

import io.javalin.Javalin;
import jodd.http.HttpStatus;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class VerbQuizzController {
	
	private VerbQuizzService verbService;
	private UserHandler userHandler;
	
	private Channel channel;
	private String queue;
	
	private ObjectMapper mapper;
	
	private static final String AUTH = "Authorization";
	
	private static final String BEARER = "Bearer";
	public void init(Javalin app) {
		
		
		app.get("/", ctx -> {
			log.info("Create quizz");
		//	ctx.contentType(APPLICATION_JSON_UTF8);
			Optional<String> user = userHandler.retrieveUser(ctx);
			
			if(user.isPresent()) {
				String token = ctx.header(AUTH).substring(BEARER.length()).trim();
				
				log.info("Retrieved user ");
				log.info(user.toString());
				
				List<VerbQuestion> list = verbService.generate(token, 10);
				list.forEach(System.out::println);
				list.forEach(q ->{
					log.debug("Create question from " + q);
					q.addAcceptedAnswer(q.getVerb().getSpanish());
					q.addAcceptedAnswer(StringUtils.stripAccents(q.getVerb().getSpanish()));
				});
				
				ctx.status(200);
				ctx.json(list);
			} else {
				log.error("Missing user");
			}
				
		});
		
		app.post("/answers", ctx -> {
			log.info("Begin save");
			Optional<String> user = userHandler.retrieveUser(ctx);
			if(user.isPresent()) {
				log.info("User : " + user.get());
				
				VerbAnswerDto[] answerBody = ctx.bodyAsClass(VerbAnswerDto[].class);
				List<VerbAnswerDto> answers = Arrays.asList(answerBody);
				ctx.status(HttpStatus.HTTP_CREATED);
				for(VerbAnswerDto a : answers){
					VerbAnswer answer = createAnswer(user, a);
					channel.basicPublish("", queue, null, mapper.writeValueAsBytes(answer));
				}
			}
		});
			
	}

	private VerbAnswer createAnswer(Optional<String> user, VerbAnswerDto a) {
		VerbAnswer answer = new VerbAnswer(a.getQuestion(),user.get());
		answer.setAnswer(a.getAnswer());
		answer.setAnswerTime(a.getAnswerTime());
		answer.setUsername(user.get());
		answer.setScore(a.getScore());
		return answer;
	}
}
