package com.notesonjava.spanish.verbs;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notesonjava.spanish.model.WordStats;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class VerbReportClient {
	
	private ReportUrlBuilder urlBuilder;
	private ObjectMapper mapper;

	public List<WordStats<String>> getReport(String token) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.report();
		HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();
		WordStats<String>[] verbs = mapper.readValue(response.bodyBytes(), WordStats[].class);
		return Arrays.asList(verbs);	
	}
	
	public List<WordStats<String>> getReport(Collection<String> verbList, String token) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.report(verbList);
		HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();
		WordStats<String>[] verbs = mapper.readValue(response.bodyBytes(), WordStats[].class);
		return Arrays.asList(verbs);	
	}
	
	
	public List<WordStats<LocalDate>> getLastWeekReport(String token) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.dailyReport();
		HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();
		WordStats<LocalDate>[] verbs = mapper.readValue(response.bodyBytes(), WordStats[].class);
		return Arrays.asList(verbs);	
	}
	
	public List<WordStats<LocalDate>> getDailyReport(int numDays, String token) throws JsonParseException, JsonMappingException, IOException{
		String url = urlBuilder.dailyReport(numDays);
		HttpResponse response = HttpRequest.get(url).tokenAuthentication(token).send();
		WordStats<LocalDate>[] verbs = mapper.readValue(response.bodyBytes(), WordStats[].class);
		return Arrays.asList(verbs);	
	}
}
