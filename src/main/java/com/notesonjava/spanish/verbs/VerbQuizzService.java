package com.notesonjava.spanish.verbs;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbQuestion;
import com.notesonjava.spanish.model.WordStats;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class VerbQuizzService {

	private VerbClient verbClient;
	private VerbReportClient reportClient;
	
	public List<VerbQuestion> generate(String username, int size) throws JsonParseException, JsonMappingException, IOException{
		List<WordStats<String>> report = reportClient.getReport(username);
		long goodVerbs = report.stream().filter(stat -> stat.getScore() > 60).count();
		List<Verb> verbList = verbClient.getPopularVerbs(goodVerbs+20);
		List<WordStats<String>> selected = generateVerbList(username, verbList, size);
		return verbClient.getSynonymsFromStats(selected);
	}

	private List<WordStats<String>> generateVerbList(String username, List<Verb> verbList, int size) throws JsonParseException, JsonMappingException, IOException {
		Set<String> nameList = verbList.stream().map(v -> v.getSpanish()).collect(Collectors.toSet());
		List<WordStats<String>> stats = reportClient.getReport(nameList, username);
		Collections.shuffle(stats);
		if(stats.size() <= size) {
			return stats;
		}else {
			return stats.subList(stats.size()- size, stats.size());
		}	
	}
	
	
	
}
