package com.notesonjava.spanish;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;

import org.bson.BsonDateTime;
import org.bson.Document;

import com.notesonjava.spanish.model.VerbAnswer;

public class DocumentAdapter {
	
	public static Document toDocument(VerbAnswer answer) {
		Document doc = new Document();
		doc.put("verb", answer.getVerb());
		doc.put("answer", answer.getAnswer());
		doc.put("username", answer.getUsername());
		doc.put("score", answer.getScore());
		doc.put("answerTime", new BsonDateTime(answer.getAnswerTime().toInstant().toEpochMilli()));
		return doc;
	}

	public static VerbAnswer toVerbAnswer(Document doc) {
		VerbAnswer answer = new VerbAnswer(doc.getString("verb"), doc.getString("username"));
		answer.setAnswer(doc.getString("answer"));
		answer.setScore(doc.getInteger("score",0));
		Date answerDate = doc.getDate("answerTime");
		Instant instant = Instant.ofEpochMilli(answerDate.getTime());
		answer.setAnswerTime(ZonedDateTime.ofInstant(instant, ZoneOffset.UTC));
		return answer;	
	}
}
