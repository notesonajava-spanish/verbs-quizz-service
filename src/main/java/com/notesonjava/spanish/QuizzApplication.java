package com.notesonjava.spanish;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.notesonjava.auth.QuizzAccessManager;
import com.notesonjava.auth.UserHandler;
import com.notesonjava.config.PropertyLoader;
import com.notesonjava.config.PropertyMap;
import com.notesonjava.spanish.verbs.ReportUrlBuilder;
import com.notesonjava.spanish.verbs.VerbClient;
import com.notesonjava.spanish.verbs.VerbQuizzController;
import com.notesonjava.spanish.verbs.VerbQuizzService;
import com.notesonjava.spanish.verbs.VerbReportClient;
import com.notesonjava.spanish.verbs.VerbUrlBuilder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import io.javalin.Javalin;
import io.javalin.json.JavalinJackson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QuizzApplication {
	
	public static final String VERB_QUEUE = "AnswersVerbs";
	
	private static Channel openChannel(Connection connection) {
		Channel channel = null;
    	
    	while(channel == null) {
		    try {
	    		channel = connection.createChannel();
		    } catch (Exception e) {
	    		log.warn(e.getMessage());
	    		try {
	    			log.warn("Wait 1 second before retrying channel");
	    			Thread.sleep(1000);
	    		} catch(Exception e2) {
	    			log.warn(e2.getMessage());
	    		}
	    	}
	    }
		return channel;
	}

	private static Connection openConnection(ConnectionFactory factory) {
		Connection connection = null;
    	while(connection == null) {
		    try {
	    		connection = factory.newConnection();		    	
		    } catch (Exception e) {
	    		log.warn(e.getMessage());
	    		try {
	    			log.warn("Wait 1 second before retrying connection");
	    			Thread.sleep(1000);
	    		} catch(Exception e2) {
	    			log.warn(e2.getMessage());
	    		}
	    	}
	    }
		return connection;
	}

	
	public static void main(String[] args) throws IOException, TimeoutException {
		PropertyMap prop = PropertyLoader.loadProperties();
		String portValue = prop.get("server.port").orElse("8080");
		String verbServiceUrl = prop.get("verb.service.url").orElse("http://localhost:8081");
		String reportServiceUrl = prop.get("report.service.url").orElse("http://localhost:8081");
		
		
		String authUserUrl = prop.get("auth.user.url").orElse("https://fmottard.auth0.com/userinfo");
		String mqHost = prop.get("mq.host").orElse("localhost");
	
    	log.info("Create the queues");
    	ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(mqHost);
    	    	
        log.info("Create the channel");
    	
        Connection connection = openConnection(factory);
	    Channel channel = openChannel(connection);
	    channel.queueDeclare(VERB_QUEUE, false, false, false, null);
	
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		JavalinJackson.configure(mapper);
		
		VerbUrlBuilder verbUrlBuilder = new VerbUrlBuilder(verbServiceUrl);
		VerbClient verbClient = new VerbClient(verbUrlBuilder, mapper);
		ReportUrlBuilder reportUrlBuilder = new ReportUrlBuilder(reportServiceUrl);
		VerbReportClient reportClient = new VerbReportClient(reportUrlBuilder, mapper);
		VerbQuizzService verbService = new VerbQuizzService(verbClient,reportClient);
		UserHandler userHandler = new UserHandler();
		
		VerbQuizzController verbController = new VerbQuizzController(verbService, userHandler, channel, VERB_QUEUE, mapper);
		
		
		Javalin app = Javalin.create()
				.enableCorsForAllOrigins()
				.port(Integer.parseInt(portValue));
		
		app.defaultContentType("application/json;charset=utf-8");
		
		app.requestLogger((ctx, timeMs) -> {
			log.info("Url: " + ctx.url());
			log.info("QueryString: " + ctx.queryString());
			log.info("Headers");
			ctx.headerMap().entrySet().forEach(entry -> log.info(entry.getKey() + ":" + entry.getValue()));
		    log.info(ctx.method() + " "  + ctx.path() + " took " + timeMs + " ms");
		});
		
		final PingResult pingResult = new PingResult("OK");
		
		app.get("/ping", ctx -> {
			log.info("Entering ping");
			ctx.json(pingResult);
		});
		
		QuizzAccessManager accessManager = new QuizzAccessManager(authUserUrl, mapper);
		
		app.accessManager((handler, ctx, permittedRoles) -> accessManager.manage(handler, ctx, permittedRoles));	
		verbController.init(app);
		
		app.start();
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
	    	public void run() {
	    		closeChannel(channel); 
	    		closeConnection(connection); 
	    	}

			private void closeConnection(Connection connection) {
				try {
	    			if(connection != null) {
	    				connection.close();
	    			}
				} catch (Exception e) {
					log.error("Unable to close connection", e);
				}
			}

			private void closeChannel(Channel channel) {
				try {
	    			if(channel != null) {
	    				channel.close();
	    			}	
				} catch (Exception e) {
					log.error("Unable to close connection", e);
				}
			}
	    });
	}
}

@Data
@AllArgsConstructor
class PingResult {
	private String status;
}

