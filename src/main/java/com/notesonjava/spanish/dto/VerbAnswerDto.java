package com.notesonjava.spanish.dto;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VerbAnswerDto {

	private String answer;
	private ZonedDateTime answerTime;
	private String question;
	private int score;
	
	public VerbAnswerDto(String question){
		this.question = question;
		answerTime = ZonedDateTime.now(ZoneOffset.UTC);
	}
}
