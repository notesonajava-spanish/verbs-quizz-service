package com.notesonjava.spanish;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.hamcrest.MockitoHamcrest;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.notesonjava.spanish.model.Verb;
import com.notesonjava.spanish.model.VerbQuestion;
import com.notesonjava.spanish.verbs.VerbClient;
import com.notesonjava.spanish.verbs.VerbQuizzService;
import com.notesonjava.spanish.verbs.VerbReportClient;

public class VerbQuizzServiceTest {
	
	
	
	
	private static String USER = "user1";
	
	private static List<String> VERB_LIST = Arrays.asList("estar", "ser", "tener", "haber", "hacer", "dar", "decir", "mendar", "manejar", "disfrutar", "llover", "poner", "poder");
	
	private VerbClient client;
	private VerbReportClient reportClient;
	private VerbQuizzService service;
	
	
	@Before
	public void setup() {
		client = Mockito.mock(VerbClient.class);
		reportClient = Mockito.mock(VerbReportClient.class);
		service = new VerbQuizzService(client, reportClient);
	}
	
	@Test
	public void test_generate() throws JsonParseException, JsonMappingException, IOException{
		
		List<Verb> verbs = new ArrayList<>();
		for(int i = 0; i< 100; i++){
			verbs.add(new Verb("myverb-"+i));
		}

		Mockito.when(client.getPopularVerbs(MockitoHamcrest.longThat(Matchers.any(Long.class)))).thenReturn(verbs);
		
		List<VerbQuestion> synonymList = new ArrayList<>();
		for(Verb v : verbs.subList(0, 10)){//10 verbs will be requested by the service.
			VerbQuestion vs = new VerbQuestion();
			vs.setVerb(v);
			synonymList.add(vs);
		}
		
		Mockito.when(reportClient.getReport(MockitoHamcrest.argThat(Matchers.equalTo(USER)))).thenReturn(new ArrayList<>());
		
		Mockito.when(reportClient.getReport(MockitoHamcrest.argThat(Matchers.any(List.class)),MockitoHamcrest.argThat(Matchers.equalTo(USER))))
			.thenReturn(new ArrayList<>());
		
		Mockito.when(client.getSynonymsFromStats(MockitoHamcrest.argThat(Matchers.any(List.class)))).thenReturn(synonymList);
		
		List<VerbQuestion> result = service.generate(USER,10);
		Assertions.assertThat(result).hasSize(10);
	}
	
	
}
