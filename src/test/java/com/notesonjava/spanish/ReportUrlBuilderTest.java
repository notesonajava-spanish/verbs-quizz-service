package com.notesonjava.spanish;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import com.notesonjava.spanish.verbs.ReportUrlBuilder;

public class ReportUrlBuilderTest {

	private ReportUrlBuilder builder = new ReportUrlBuilder("http://dummy.com");
	
	@Test
	public void testVerbReportUrl() {
		String url = builder.report();
		Assertions.assertThat(url).isEqualTo("http://dummy.com/verbs/report");
	}

	@Test
	public void testWeekReportUrl() {
		String url = builder.dailyReport();
		Assertions.assertThat(url).isEqualTo("http://dummy.com/verbs/report/daily");
	}


	@Test
	public void testDailyReportUrl() {
		String url = builder.dailyReport(10);
		Assertions.assertThat(url).isEqualTo("http://dummy.com/verbs/report/daily?numDays=10");
	}
}
